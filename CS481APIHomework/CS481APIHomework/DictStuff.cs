﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;


namespace CS481APIHomework
{
    public class Definitions
    {
        public List<DefStuff> Defs
        {
            get;
            set;
        }
    }
    public class DefStuff
    { 
        [JsonProperty("type")]
        public string Type
        {
            get;
            set;
        }
        [JsonProperty("definition")]
        public string Definition
        {
            get;
            set;
        }
        [JsonProperty("example")]
        public string Example
        {
            get;
            set;
        }
    }
}
