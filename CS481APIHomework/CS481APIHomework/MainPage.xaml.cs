﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Xamarin.Forms;
using Plugin.Connectivity;

namespace CS481APIHomework
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        async void Button_Click(object sender, System.EventArgs e)
        {
            
            if(!CrossConnectivity.Current.IsConnected)
            {
                await DisplayAlert("Alert!","No Internet Connection!","Okay");
                return;
            }

            var client = new HttpClient();

            var dictApiAddress = "https://owlbot.info/api/v2/dictionary/" + Entry + "?format=json";
            var uri = new Uri(dictApiAddress);


            Definitions definition = new Definitions();
            var response = await client.GetAsync(uri);
            if(response.IsSuccessStatusCode)
            {
                var jsonContent = await response.Content.ReadAsStringAsync();
                definition = JsonConvert.DeserializeObject<Definitions>(jsonContent);
            }
            dictList.ItemsSource = new ObservableCollection<DefStuff>(definition.Defs);
        }
    }
}
